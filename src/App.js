import logo from './logo.svg';
import React, {Component} from 'react';
import './App.css';
import Number from "./numberComponent/number";
import {randomArray} from "./numberComponent/RandomArray";

class App extends Component{
    state = {
    numbers: randomArray(5)
  };

  changeNumbers = () => {
    this.setState({numbers: randomArray(5)});
  };

  render ()
  {
    return (
        <div className="App">
          <div className="btn ь">
            <button onClick={this.changeNumbers}>New numbers</button>
          </div>
          <div>
            <Number number={this.state.numbers[0]} />
            <Number number={this.state.numbers[1]} />
            <Number number={this.state.numbers[2]} />
            <Number number={this.state.numbers[3]} />
            <Number number={this.state.numbers[4]} />
          </div>

        </div>
    );
  }
}

export default App;
