import React from 'react';
import './number.css';


const Number = props => {
    return (
        <div className="circle">
            <div className="number">{props.number}</div>
        </div>
    );
}

export default Number;